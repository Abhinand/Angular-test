import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private userService: UserService) { }

  getInfo(){
  	this.userService.getMyInfo().subscribe(
  		data => {console.log(data)},
    	error => {
    		console.log("errpr",error.status)
    	}
  	);
  }
  ngOnInit() {
  }

}
