import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
 
import { ApiService } from './api.service';
import { ConfigService } from './config.service';

@Injectable()
export class AuthService {
  
  private authManager_: BehaviorSubject<AuthState> =
            new BehaviorSubject(AuthState.LoggedOut);
  private authState_: AuthState;
  authChange: Observable<AuthState>;
  
  constructor(
    private apiService: ApiService,
    //private userService: UserService,
    private config: ConfigService
  ) {
    this.authChange = this.authManager_.asObservable();
   }

  login(user) {
    const body = `username=${user.username}&password=${user.password}`;
    const headers = new Headers();
	headers.append('Content-Type', 'application/x-www-form-urlencoded');

    console.log('header passed is >>>'+JSON.stringify(headers));
    return this.apiService.post(this.config.login_url, body, headers);
  }

  logout() {
    return this.apiService.post(this.config.logout_url, {})
  	.map(() => {
    /*this.userService.currentUser = null;*/
  });
  }

  emitAuthState():void {
    this.authManager_.next(this.authState_);
  }

  setAuthState_(flag: boolean) {
    if(flag) {
    this.authState_ = AuthState.LoggedIn;  
    }else {
      this.authState_ = AuthState.LoggedOut;
    }
    
    this.emitAuthState();
  }
}

export const enum AuthState {
  LoggedIn,
  LoggedOut
}