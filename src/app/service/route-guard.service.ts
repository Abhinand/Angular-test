import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { AuthService, AuthState } from '../service/auth.service';

@Injectable()
export class RouteGuardService implements CanActivate {

	constructor(private authService: AuthService,
							private router: Router ){}

	canActivate(): Observable<boolean>{
		/*this method is invoked during router changes if this class is listed in the routes*/
		return this.authService.authChange.map(
			authState =>{
				if(authState === AuthState.LoggedIn) {
					return true;
				}else {
					this.router.navigate(['login']);
				}
			}
			);
	}

}