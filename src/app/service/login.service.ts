import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {
	private authManager_: BehaviorSubject<AuthState> =
						new BehaviorSubject(AuthState.LoggedOut);
	private authState_: AuthState;
	authChange: Observable<AuthState>;

	constructor() {
		this.authChange = this.authManager_.asObservable();
	}
}

export const enum AuthState {
	LoggedIn,
	LoggedOut
}
