import { Component, OnDestroy} from '@angular/core';
import { AuthService, AuthState } from './service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy{
  title = 'app';
  loggedIn: boolean;
  private authChangeSubscription: Subscription;

  constructor(private authService: AuthService) {
  	this.authChangeSubscription = authService.authChange.subscribe(
  		newAuthState => this.loggedIn= (newAuthState===AuthState.LoggedIn)
  	);
  }

  ngOnDestroy(): void {
  	this.authChangeSubscription.unsubscribe();
  }
  /*isUserLoggedIn() {
	this.userService.isUserLogin();
  }*/
}
