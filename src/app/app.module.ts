import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

/*Router module */
import { AppRouterModule } from './common/app-router.module';

/*main custom components starts*/
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { HeaderComponent } from './mainComponents/header/header.component';
import { SideNavComponent } from './mainComponents/side-nav/side-nav.component';
import { MainContentComponent } from './mainComponents/main-content/main-content.component';
import { PageNotFoundComponent } from './common/component/page-not-found/page-not-found.component';

/*module elements starts*/
import { PatientRegComponent } from './patientModule/registration/patient-reg.component';
import { HomeComponent } from './mainComponents/home/home.component';

/*import for custom services starts*/
import { ConfigService, UserService, AuthService, ApiService } from './service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SideNavComponent,
    PageNotFoundComponent,
    PatientRegComponent,
    MainContentComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ConfigService,
    UserService, 
    AuthService,
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
