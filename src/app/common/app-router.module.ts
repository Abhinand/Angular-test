import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from '../login/login.component';
import { PatientRegComponent } from '../patientModule/registration/patient-reg.component';
import { HomeComponent } from '../mainComponents/home/home.component';
import { PageNotFoundComponent } from '../common/component/page-not-found/page-not-found.component';

import { RouteGuardService } from '../service';

const routes: Routes = [
	{ path:'', component: HomeComponent,pathMatch: 'full', canActivate: [RouteGuardService]},
	{ path:'login', component: LoginComponent},
	{ path:'home', component: HomeComponent, canActivate: [RouteGuardService]},
	{ path:'patients', component: PatientRegComponent, canActivate: [RouteGuardService]},
	{ path: '**', component: PageNotFoundComponent , canActivate: [RouteGuardService]}
];

@NgModule({
	imports: [RouterModule.forRoot(routes,{ enableTracing: true } // <-- debugging purposes only
		)],
	exports: [RouterModule],
	providers:[RouteGuardService]
})
export class AppRouterModule {

}